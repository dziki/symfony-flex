<?php
/**
 * Created by PhpStorm.
 * User: Artur
 * Date: 22.11.2017
 * Time: 00:32
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Unirest\Request as ApiRequest;

class DefaultController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $headers = ['Accept' => 'application/json'];
        $query = [
            'key' => getenv('STEAM_API_KEY'),
            'steamids' => $request->request->get('steamId'),
        ];
        $response = ApiRequest::get('http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/', $headers, $query);
        return $this->json($response);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function inventory(Request $request)
    {
        $headers = ['Accept' => 'application/json'];
        $query = [
            'l' => 'english',
            'count' => $request->get('itemsCount'),
        ];
        $response = ApiRequest::get('http://steamcommunity.com/inventory/'.$request->get('steamId').'/440/2', $headers, $query);
        return $this->json($response);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function ownedGames(Request $request)
    {
        $headers = ['Accept' => 'application/json'];
        $query = [
            'key' => getenv('STEAM_API_KEY'),
            'steamid' => $request->get('steamId'),
            'format' => 'json',
        ];
        $response = ApiRequest::get('http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/', $headers, $query);
        return $this->json($response);
    }
}